# React CRUD App

## Functionality
* Create user
* View created user
* Update user
* Delete user

## Tools used
* React
* Flux
* React router
* Bootstrap
* Sass
* Webpack
    
## To run the app run: 
* yarn install
* yarn start
* App will be served in http://localhost:3000/
