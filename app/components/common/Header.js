import React, { Component } from 'react';
import { Link } from 'react-router';

export default class Header extends Component{
    constructor(props){
        super(props);

        this.state={};
    }

    render(){
        return(
            <header>
                <div className="container clearifx">
                    <h1 className="branding pull-left"><Link to="/">React CRUD App</Link></h1>
                    <div className="nav-holder pull-right">
                        <Link to="/user/create" className="nav-text" activeClassName="active">
                            <i className="fa fa-user"/>Create User
                        </Link>
                        <Link to="/user/list" className="nav-text" activeClassName="active">
                            <i className="fa fa-list"/>List users
                        </Link>
                    </div>
                </div>
            </header>
        )
    }

}