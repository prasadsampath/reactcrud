import React, { Component } from 'react';
import { Link } from 'react-router';
import { Modal } from 'react-bootstrap';

import AppStore from 'stores/AppStore';
import { createUser, closeUserCreateResponse } from 'actions/UserAction';

export default class Index extends Component{
    constructor(props){
        super(props);

        this.state={
            userInfo :{
                fName: '',
                lName: '',
                email: '',
            },
            isManditoryFieldsValid: true,
            isEmailValid: true,
            userCreateResponse: null,
            isAlertVisible: false,
            isLoadingVisible: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }
  
    componentDidMount() {
        AppStore.addChangeListener(this.handleChange);
    }
  
    componentWillUnmount() {
        AppStore.removeChangeListener(this.handleChange);
    }
  
    handleChange() {
  
      this.setState({
        userCreateResponse: AppStore.userCreateStatus(),
        isAlertVisible: AppStore.showUserCreatePopup(),
        isLoadingVisible: AppStore.isLoadingVisible(),
      });

    }

    close() {
        this.setState({ isAlertVisible : false }, function(){
            closeUserCreateResponse();
        });
    }

    alertPopup(){
        const {userCreateResponse} = this.state;

        return(
        <Modal show={this.state.isAlertVisible} className="alert-modal">
            <div className="model-inner-holder">                
                {
                    (userCreateResponse)?
                    <div className="modal-header-holder">
                        <h3 className="title">User created Successfully!</h3>
                        <div className="btn-holder clearfix">
                            <Link to="/user/list" className="btn btn-primary pull-right">Okay</Link>
                        </div>
                    </div>
                    :
                    <div className="modal-header-holder">
                        <h3 className="title">User create failed, Please try again.</h3>
                        <div className="btn-holder clearfix">
                            <button className="btn btn-primary pull-right" onClick={() => this.close()}>Okay</button>
                        </div>
                    </div>
                }
            </div>
        </Modal>
        )
    }

    validateEmail(mail) {
        if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
          return true;
        } else {
          return false;
        }
    }

    handleInput(val, field){
        const {userInfo} = this.state;
        
        userInfo[field] = val;

        this.setState({userInfo});
    }

    handleUserCreate(e){
        const {userInfo} = this.state;
        e.preventDefault();
        
        if(userInfo.fName.length == 0 || userInfo.lName.length == 0){
            this.setState({isManditoryFieldsValid : false});            
            return false;
        }else{
            this.setState({isManditoryFieldsValid : true});
        }
        
        if(!this.validateEmail(userInfo.email)){
            this.setState({isEmailValid : false});
            return false;
        }else{
            this.setState({isEmailValid : true});
        }

        let data = {
            "firstName": userInfo.fName,
            "lastName": userInfo.lName,
            "email": userInfo.email
        }
        
        createUser(data);
    }

    render(){
        const{userInfo, isManditoryFieldsValid, isEmailValid, isLoadingVisible} = this.state;

        return(
            <section className="main-container create-user-container">
                {
                    (isLoadingVisible)?
                    <span className="boxLoading"/>
                    :
                    <div className="container">
                        <h2>Create User</h2>
                        <div className="form-holder col-sm-6 col-sm-offset-3">
                            <form noValidate onSubmit={(e) => this.handleUserCreate(e)}>
                                <div className="form-group row clearfix">
                                    <label className="col-sm-4 col-form-label">First Name *</label>
                                    <div className="col-sm-6 col-xs-6 pull-right">
                                        <input type="text" className="form-control" value={userInfo.fName} onChange={(e) => this.handleInput(e.target.value, 'fName')} placeholder="First Name" />
                                    </div>
                                </div>
                                <div className="form-group row clearfix">
                                    <label className="col-sm-4 col-form-label">Last Name*</label>
                                    <div className="col-sm-6 col-xs-6 pull-right">
                                        <input type="text" className="form-control" value={userInfo.lName} onChange={(e) => this.handleInput(e.target.value, 'lName')} placeholder="Last Name" />
                                    </div>
                                </div>
                                <div className="form-group row clearfix">
                                    <label className="col-sm-4 col-form-label">Email*</label>
                                    <div className="col-sm-6 col-xs-6 pull-right">
                                        <input type="text" className="form-control" value={userInfo.email} onChange={(e) => this.handleInput(e.target.value, 'email')} placeholder="Email" />
                                    </div>
                                </div>
                                <small>* mandetory fields</small>
                                {
                                (!isManditoryFieldsValid)?
                                <span className="validate error-msg">Please fill all mandetory fields.</span>
                                :
                                null
                                }
                                {
                                (!isEmailValid)?
                                <span className="validate error-msg">Please add valid email address.</span>
                                :
                                null
                                }
                                <div className="btn-holder clearfix">
                                    <button type="submit" id="submitBtn" className="btn btn-primary pull-right">Submit</button>
                                </div>
                            </form>
                        </div>
                        {this.alertPopup()}
                    </div>
                }
            </section>
        )
    }

}