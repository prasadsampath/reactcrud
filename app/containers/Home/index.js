import React, { Component } from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';

export default class Index extends Component{
    constructor(props){
        super(props);

        this.state={};
    }


    render(){
        return(
            <section className="main-container">                
                <div className="container">
                    <h2>Home</h2>
                    <p>Use this application to create, Read, Update and Delete users.</p>
                </div>
            </section>
        )
    }

}