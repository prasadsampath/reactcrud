import React, { Component } from 'react';
import {Nav, Navbar, NavItem, Modal} from 'react-bootstrap';

import AppStore from 'stores/AppStore';
import { listUsers, viewUser, updateUser, closeUserCreateResponse, deleteUser } from 'actions/UserAction';

export default class Index extends Component{
    constructor(props){
        super(props);

        this.state={
            viewUserData: null,
            userList: null,
            isLoadingVisible: false,
            isUserPopupVisible: false,
            isEditUserPopupVisible: false,
            isPoupDataLoading: false,
            selectedUser: null,
            isManditoryFieldsValid: true,
            isEmailValid: true,
            userCreateResponse: null,
            isAlertVisible: false,
            userDeleteResponse: null,
            isDeleteAlertVisible: false,
        };

        this.handleChange = this.handleChange.bind(this);
    }
  
    componentDidMount() {
        AppStore.addChangeListener(this.handleChange);
        listUsers();
    }
  
    componentWillUnmount() {
        AppStore.removeChangeListener(this.handleChange);
    }
  
    handleChange() {
  
      this.setState({
          userList: AppStore.listUsers(),
          isLoadingVisible: AppStore.isLoadingVisible(),
          viewUserData: AppStore.loadUserData(),
          isPoupDataLoading: AppStore.isPopupDataLoadingVisible(),
          userCreateResponse: AppStore.userCreateStatus(),
          isAlertVisible: AppStore.showUserCreatePopup(),
          userDeleteResponse: AppStore.userDeleteStatus(),
          isDeleteAlertVisible: AppStore.showUserDeletedPopup(),
      });

    }

    close() {
        this.setState({ isUserPopupVisible : false, isEditUserPopupVisible : false, isAlertVisible : false, isDeleteAlertVisible : false }, function(){
            closeUserCreateResponse();
        });
    }

    validateEmail(mail) {
        if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
          return true;
        } else {
          return false;
        }
    }

    handleInput(val, field){
        let {selectedUser} = this.state;
        
        selectedUser[field] = val;

        this.setState({selectedUser});
    }

    handleUserCreate(e){
        const {selectedUser} = this.state;
        e.preventDefault();
        
        if(selectedUser.fName.length == 0 || selectedUser.lName.length == 0){
            this.setState({isManditoryFieldsValid : false});            
            return false;
        }else{
            this.setState({isManditoryFieldsValid : true});
        }
        
        if(!this.validateEmail(selectedUser.email)){
            this.setState({isEmailValid : false});
            return false;
        }else{
            this.setState({isEmailValid : true});
        }

        let data = {
            "firstName": selectedUser.fName,
            "lastName": selectedUser.lName,
            "email": selectedUser.email
        }
        
        this.setState({isManditoryFieldsValid : true, 
            isEmailValid : true, selectedUser : null, 
            isEditUserPopupVisible : false}, function(){
            updateUser(data);
        });
    }

    viewUserPopup(){
        const { isPoupDataLoading, viewUserData } = this.state;

        return(
        <Modal show={this.state.isUserPopupVisible} className="alert-modal">
            <div className="model-inner-holder">
                {
                    (isPoupDataLoading)?
                    <div className="modal-header-holder">
                        <span className="boxLoading"/>
                    </div>
                    :
                        (viewUserData != null)?
                        <div className="modal-header-holder">
                            <h3 className="title">User information</h3>
                            <div className="modal-body-holder">
                                <p><span className="label">First Name:</span> {viewUserData.firstName}</p>
                                <p><span className="label">Last Name: </span> {viewUserData.lastName}</p>
                                <p><span className="label">Email:</span> {viewUserData.email}</p>
                            </div>
                            <div className="btn-holder clearfix">
                                <button className="btn btn-primary pull-right" onClick={() => this.close()}>Okay</button>
                            </div>
                        </div>
                        :
                        null
                }
                <span className="fa fa-times close" onClick={() => this.close()}/>
            </div>
        </Modal>
        )
    }

    closeEditPopup(){
        this.setState({isManditoryFieldsValid : true, isEmailValid : true, selectedUser : null, isEditUserPopupVisible : false});
    }
    
    editUserPopup(){
        const { isPoupDataLoading, selectedUser, isManditoryFieldsValid, isEmailValid } = this.state;

        return(
        <Modal show={this.state.isEditUserPopupVisible} className="info-modal">
            <div className="model-inner-holder">
                <div className="modal-header-holder">
                    <h3 className="title">User information</h3>
                    <div className="modal-body-holder">
                        {
                            (selectedUser != null)?
                            <form noValidate onSubmit={(e) => this.handleUserCreate(e)}>
                                <div className="form-group row clearfix">
                                    <label className="col-sm-4 col-form-label">First Name *</label>
                                    <div className="col-sm-6 col-xs-6 pull-right">
                                        <input type="text" className="form-control" value={selectedUser.fName} onChange={(e) => this.handleInput(e.target.value, 'fName')} placeholder="First Name" />
                                    </div>
                                </div>
                                <div className="form-group row clearfix">
                                    <label className="col-sm-4 col-form-label">Last Name*</label>
                                    <div className="col-sm-6 col-xs-6 pull-right">
                                        <input type="text" className="form-control" value={selectedUser.lName} onChange={(e) => this.handleInput(e.target.value, 'lName')} placeholder="Last Name" />
                                    </div>
                                </div>
                                <div className="form-group row clearfix">
                                    <label className="col-sm-4 col-form-label">Email*</label>
                                    <div className="col-sm-6 col-xs-6 pull-right">
                                        <input type="text" className="form-control" value={selectedUser.email} onChange={(e) => this.handleInput(e.target.value, 'email')} placeholder="Email" />
                                    </div>
                                </div>
                                <small>* mandetory fields</small>
                                {
                                    (!isManditoryFieldsValid)?
                                        <span className="validate error-msg">Please fill all mandetory fields.</span>
                                    :
                                        null
                                    }
                                {
                                    (!isEmailValid)?
                                        <span className="validate error-msg">Please add valid email address.</span>
                                    :
                                        null
                                }
                                <div className="btn-holder clearfix">
                                    <button className="btn btn-primary pull-left" onClick={() => this.closeEditPopup()}>Close</button>
                                    <button type="submit" id="submitBtn" className="btn btn-primary pull-right">Submit</button>
                                </div>
                            </form>
                            :
                            null
                        }
                    </div>
                </div>
            </div>
        </Modal>
        )
    }

    loadUserData(){
        this.setState({isUserPopupVisible : true}, function(){
            viewUser("5a5630d51434791cdc22c8e1");//hard coded user ID since there's no ID in the user object
        });
    }
    
    editUserData(rowID){
        let {userList} = this.state;

        let data = {
            fName: userList[rowID].firstName,
            lName: userList[rowID].lastName,
            email: userList[rowID].email,
        }

        this.setState({selectedUser: data, isEditUserPopupVisible : true});
    }

    alertPopup(){
        const {userCreateResponse} = this.state;

        return(
        <Modal show={this.state.isAlertVisible} className="alert-modal">
            <div className="model-inner-holder">                
                {
                    (userCreateResponse)?
                    <div className="modal-header-holder">
                        <h3 className="title">User created Successfully!</h3>
                        <div className="btn-holder clearfix">
                            <button className="btn btn-primary pull-right" onClick={() => this.close()}>Okay</button>
                        </div>
                    </div>
                    :
                    <div className="modal-header-holder">
                        <h3 className="title">User create failed, Please try again.</h3>
                        <div className="btn-holder clearfix">
                            <button className="btn btn-primary pull-right" onClick={() => this.close()}>Okay</button>
                        </div>
                    </div>
                }
            </div>
        </Modal>
        )
    }
    
    deleteAlertPopup(){
        const {userDeleteResponse} = this.state;

        return(
        <Modal show={this.state.isDeleteAlertVisible} className="alert-modal">
            <div className="model-inner-holder">                
                {
                    (userDeleteResponse)?
                    <div className="modal-header-holder">
                        <h3 className="title">User deleted Successfully!</h3>
                        <div className="btn-holder clearfix">
                            <button className="btn btn-primary pull-right" onClick={() => this.close()}>Okay</button>
                        </div>
                    </div>
                    :
                    <div className="modal-header-holder">
                        <h3 className="title">User delete failed, Please try again.</h3>
                        <div className="btn-holder clearfix">
                            <button className="btn btn-primary pull-right" onClick={() => this.close()}>Okay</button>
                        </div>
                    </div>
                }
            </div>
        </Modal>
        )
    }

    render(){
        const {userList, isLoadingVisible} = this.state;
        let tableRow,
            _this = this;

        if(userList != null){
            tableRow = userList.map(function(row, key){
                return (
                    <tr key={key}>
                        <td>{row.firstName}</td>
                        <td>{row.lastName}</td>
                        <td>{row.email}</td>
                        <td>
                            <i className="fa fa-eye" onClick={() => _this.loadUserData()}/>
                            <i className="fa fa-edit" onClick={() => _this.editUserData(key)}/>
                            <i className="fa fa-times" onClick={() => deleteUser("5a5630d51434791cdc22c8e1")}/>
                        </td>
                    </tr>
                )
            });
        }

        return(
            <section className="main-container list-user-container">
                {
                    (isLoadingVisible)?
                    <span className="boxLoading"/>
                    :
                    <div className="container">
                        <h2>User List</h2>
                        <table className="user-list table">
                            <thead>
                                <tr>
                                    <th>Firstname</th>
                                    <th>Lastname</th>
                                    <th>Email</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {tableRow}
                            </tbody>
                        </table>
                    </div>
                }
                {this.viewUserPopup()}
                {this.editUserPopup()}
                {this.alertPopup()}
                {this.deleteAlertPopup()}
            </section>
        )
    }

}