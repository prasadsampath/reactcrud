import PropTypes from 'prop-types';
import React from 'react';

import Header from 'components/common/Header';

const App = (props) => (
  <section className="app-container">
    <Header/>
    { React.Children.toArray(props.children) }
  </section>
);

App.propTypes = {
  children: PropTypes.node
};

export default App;
