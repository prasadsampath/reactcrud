import React from 'react';
import { IndexRoute, Router as ReactRouter, Route, Redirect } from 'react-router';

import App from 'containers/App';
import Home from 'containers/Home';
import CreateIndex from 'containers/CreateUser';
import ListIndex from 'containers/List';

const Router = (props) => (
  <ReactRouter {...props}>
    <Route path="/" component={ App }>
      <IndexRoute component={ Home }/>
      <Route path="user/create" component={ CreateIndex }/>
      <Route path="user/list" component={ ListIndex }/>
      <Redirect from='*' to='/'/>
    </Route>
  </ReactRouter>
);

export default Router;