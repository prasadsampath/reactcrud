import { AppDispatch, AppDispatchAsync }  from 'dispatchers/AppDispatcher';
import { POST, GET, PUT, DELETE } from 'utils/APIService';

import {
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILED,
  LOAD_USER,
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILED,  
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED,
  LIST_USER,
  LIST_USER_SUCCESS,
  LIST_USER_FAILED,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILED,
  CLOSE_USER_CREATE_RESPONSE_ALERT,
} from 'constants/AppConstants';

export const createUser = (data) => {
  let promise = POST('mock-user', true, data);
  AppDispatchAsync(promise, {
    request: CREATE_USER,
    success: CREATE_USER_SUCCESS,
    failure: CREATE_USER_FAILED
  }, {});
};

export const listUsers = () => {
  let promise = GET('mock-user/list', true);
  AppDispatchAsync(promise, {
    request: LIST_USER,
    success: LIST_USER_SUCCESS,
    failure: LIST_USER_FAILED
  }, {});
};

export const viewUser = (id) => {
  let promise = GET('mock-user/'+id, true);
  AppDispatchAsync(promise, {
    request: LOAD_USER,
    success: LOAD_USER_SUCCESS,
    failure: LOAD_USER_FAILED
  }, {});
};

export const updateUser = (data) => {
  let promise = PUT('mock-user', true, data);
  AppDispatchAsync(promise, {
    request: UPDATE_USER,
    success: UPDATE_USER_SUCCESS,
    failure: UPDATE_USER_FAILED
  }, {});
};

export const deleteUser = (id) => {
  let promise = DELETE('mock-user/'+id, true);
  AppDispatchAsync(promise, {
    request: DELETE_USER,
    success: DELETE_USER_SUCCESS,
    failure: DELETE_USER_FAILED
  }, {});
};

export const closeUserCreateResponse = () => {
  AppDispatch(
    CLOSE_USER_CREATE_RESPONSE_ALERT,
    {}
  );
};

