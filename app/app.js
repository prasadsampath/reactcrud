import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';

import 'assets/styles/styles.scss';

import Router from './routes.js';

ReactDOM.render(
  <Router history={ browserHistory }/>,
  document.getElementById('app')
);


