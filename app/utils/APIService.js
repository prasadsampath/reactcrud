import $ from 'jquery';
import { BASE_URL, API_KEY, API_SECRET } from "constants/AppConstants";

export function GET(url, requireAuth, additionalOptions) {
    let _this = this;

    let ajaxOptions = {
        url: BASE_URL + url,
        method: "GET",
        headers: {},
        data: additionalOptions,
    }
    if (requireAuth) {
        ajaxOptions.headers['api-key'] = API_KEY;
        ajaxOptions.headers['api-secret'] = API_SECRET;
        ajaxOptions.headers['content-type'] = "application/json";
    }

    if (additionalOptions) {
        Object.assign(ajaxOptions, additionalOptions);
    }

    return $.ajax(ajaxOptions)
}

export function POST(url, requireAuth, data, additionalOptions) {
    let _this = this

    let ajaxOptions = {
        url: BASE_URL + url,
        method: "POST",
        data: data,
        headers: {}
    }

    if (requireAuth) {
        ajaxOptions.headers['api-key'] = API_KEY;
        ajaxOptions.headers['api-secret'] = API_SECRET;
    }

    if (additionalOptions) {
        Object.assign(ajaxOptions, additionalOptions);
    }

    return $.ajax(ajaxOptions)
}

export function DELETE(url, requireAuth, additionalOptions) {
    let _this = this;

    let ajaxOptions = {
        url: BASE_URL + url,
        method: "DELETE",
        headers: {},
        data: additionalOptions,
    }
    if (requireAuth) {
        ajaxOptions.headers['api-key'] = API_KEY;
        ajaxOptions.headers['api-secret'] = API_SECRET;
        ajaxOptions.headers['content-type'] = "application/json";
    }

    if (additionalOptions) {
        Object.assign(ajaxOptions, additionalOptions);
    }

    return $.ajax(ajaxOptions)
}

export function PUT(url, requireAuth, data, additionalOptions) {
    let _this = this
    
    let ajaxOptions = {
        url: BASE_URL + url,
        method: "PUT",
        data: data,
        headers: {},
    }

    if (requireAuth) {
        ajaxOptions.headers['api-key'] = API_KEY;
        ajaxOptions.headers['api-secret'] = API_SECRET;
        ajaxOptions.headers['content-type'] = "application/json";
    }

    if (additionalOptions) {
        Object.assign(ajaxOptions, additionalOptions);
    }

    return $.ajax(ajaxOptions)
}