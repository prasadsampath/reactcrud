import { Dispatcher } from 'flux';

const flux = new Dispatcher();

export function register(callback) {
  return flux.register(callback);
}

export function waitFor(ids) {
  return flux.waitFor(ids);
}

/**
 * Dispatches a single action.
 */
export function AppDispatch(actionType, action = {}) {
  if (!actionType) {
    throw new Error('You forgot to specify type.');
  }

  flux.dispatch({ actionType, action });
}

/**
 * Dispatches three actions for an async operation represented by promise.
 */
export function AppDispatchAsync(promise, actionType, action = {}) {
  const { request, success, failure } = actionType;
  
  // console.log('**', request, success, failure, actionType, action);

  flux.dispatch({ actionType : request, action });

  promise.then(
    (body) => flux.dispatch({actionType : success, action, body}),
    (error) => flux.dispatch({actionType : failure, action, error})
  )
}
