import { register } from 'dispatchers/AppDispatcher';
import {EventEmitter} from 'events';
import { browserHistory } from 'react-router';

import {
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILED,
  LOAD_USER,
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILED,  
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED,
  LIST_USER,
  LIST_USER_SUCCESS,
  LIST_USER_FAILED,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILED,
  CLOSE_USER_CREATE_RESPONSE_ALERT,
} from 'constants/AppConstants';

let _userData = null,
    _userCreateResponsePopup = null,
    _userCreateResponseStatus = null,
    _userList = null,
    _isLoading = false,
    _isLoadingPopupData = false,
    _userDeleteResponsePopup = null,
    _userDeleteResponseStatus = null;

function startLoading(){
  _isLoading = true;
}

function startLoadingPopupData(){
  _isLoadingPopupData = true;
}

function getCreatedUser(status){
  if(status){
    _userCreateResponseStatus = true;
  }else{
    _userCreateResponseStatus = false;
  }

  _userCreateResponsePopup = true;
  _isLoading = false;
}

function deleteUser(status){
  if(status){
    _userDeleteResponseStatus = true;
  }else{
    _userDeleteResponseStatus = false;
  }

  _userDeleteResponsePopup = true;
  _isLoading = false;
}

function closeUserCreatePopup(){
  _userCreateResponseStatus = null;
  _userCreateResponsePopup = null;
  _userDeleteResponseStatus = null;
  _userDeleteResponsePopup = null;
}

function getUserList(list){
  _userList = list.data;
  _isLoading = false;
}

function getUserData(data){
  _userData = data;
  _isLoadingPopupData = false;
}

const AppStore = Object.assign({}, EventEmitter.prototype, {
  isLoadingVisible: function(){
    return _isLoading;
  },
  isPopupDataLoadingVisible: function(){
    return _isLoadingPopupData;
  },
  showUserCreatePopup: function(){
    return _userCreateResponsePopup;
  },
  userCreateStatus: function(){
    return _userCreateResponseStatus;
  },
  showUserDeletedPopup: function(){
    return _userDeleteResponsePopup;
  },
  userDeleteStatus: function(){
    return _userDeleteResponseStatus;
  },
  listUsers: function(){
    return _userList;
  },
  loadUserData: function(){
    return _userData;
  },
  addChangeListener: function (callback) {
    this.on('change', callback);
  },
  removeChangeListener: function (callback) {
    this.removeListener('change', callback);
  },
  emitChange: function () {
    this.emit('change');
  }
});

register(function (payload) {
  const {actionType, action, body} = payload;

  switch (actionType) {
    case CREATE_USER:
      startLoading();
    break;
    case CREATE_USER_SUCCESS:
      getCreatedUser(true);
    break;
    case CREATE_USER_FAILED:
      getCreatedUser(false);
    break;
    case CLOSE_USER_CREATE_RESPONSE_ALERT:
      closeUserCreatePopup(false);
    break;
    case LIST_USER:
      startLoading();
    break;
    case LIST_USER_SUCCESS:
      getUserList(body.data.mockUsers);
    break;
    case LOAD_USER:
      startLoadingPopupData();
    break;
    case LOAD_USER_SUCCESS:
      getUserData(body.data.mockUser);
    break;
    case UPDATE_USER:
      startLoading();
    break;
    case UPDATE_USER_SUCCESS:
      getCreatedUser(true);
    break;
    case UPDATE_USER_FAILED:
      getCreatedUser(false);
    break;
    case DELETE_USER:
      startLoading();
    break;
    case DELETE_USER_SUCCESS:
      deleteUser(true);
    break;
    case DELETE_USER_FAILED:
      deleteUser(false);
    break;
  }

  AppStore.emitChange();

  return true;
});

export default AppStore;